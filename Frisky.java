abstract class Member{
    private int varsta;
    private String nume;

    public Member(String nume, int varsta){
        this.nume = nume;
        this.varsta = varsta;
    }

    public  String toString(){
        String s = "Nume: " + nume + " Varsta: " + varsta + "\n";
        return s;
    }
}

class Programator extends Member{
    public Programator(String nume, int varsta){
        super(nume, varsta);
    }
}

class Manager extends Programator{
    public Manager(String nume, int varsta){
        super(nume, varsta);
    }
}

interface Risky{
    public double getRisk();
}

abstract class Project implements Risky{
    private Manager manager;
    protected Member[] members;
    private String titlu, obiectiv;
    protected long fonduri;
    protected int contor, max_size=2;

    public Project(String titlu, String obiectiv, long fonduri,  Manager manager){
        this.titlu = titlu;
        this.obiectiv = obiectiv;
        this.manager = manager;
        this.fonduri = fonduri;
        members = new Member[max_size];
        contor = 0;
    }

    public void addMember(Member m){
        if(contor<max_size)
        {
            members[contor++] = m;
        }
        else
        return;
    }
}

abstract class ProjectTunat extends Project{
    private String deadline;
    public ProjectTunat(String titlu, String obiectiv, long fonduri, Manager manager, String deadline){
        super(titlu, obiectiv, fonduri, manager);
        this.deadline = deadline;
    }
}

class Militar extends ProjectTunat{
    private String parola;
    public Militar(String titlu, String obiectiv, long fonduri, Manager manager, String deadline, String parola){
        super(titlu, obiectiv, fonduri,  manager, deadline);
        this.parola = parola;
    }
    public double getRisk(){
        double risc;
        risc = contor / parola.length() / fonduri;
        return risc;
    }
}

class Comercial extends ProjectTunat{
    private long fonduriMarc;
    private int echipe;

    public Comercial(String titlu, String obiectiv, long fonduri, Manager manager, String deadline){
        super(titlu, obiectiv, fonduri,  manager, deadline);
        fonduriMarc = fonduri/2;
    }

    public double getRisk(){
        double r;
        r = (echipe*3) / contor / (fonduri - fonduriMarc);
        return r;
    }
}

class OpenSurs extends Project{
    private String mailing_list;

    public OpenSurs(String titlu, String obiectiv, long fonduri, Manager manager,String mailing_list){
        super(titlu, obiectiv, fonduri,  manager);
        this.mailing_list = mailing_list;
    }

    public void addMember(Member m){
        if(contor<max_size){
            members[contor++] = m;
        }
        else
        {
            max_size += 2;
            Member[] aux = new Member[max_size];
            for(int i=0; i<contor; i++)
                aux[i] = members[i];
            aux[contor++] = m;
            members = aux;
        }
    }

    public String toString(){
        String s = "Membri sunt: ";
        for(int i=0; i<contor; i++){
                s+=members[i].toString();
        }
        return s;
    }

    public double getRisk(){
        double r;
        r = contor/fonduri;
        return r;
    }
}

class InvestmentCompany

class Frisky{
    public static void main(String[] args){
        Manager manager1 = new Manager("Costel", 45);
        Project p1 = new OpenSurs("Proiectul", "Dominatie Mondiala", 783643543, manager1, "Ana Ion Cristi");
        p1.addMember(manager1);
        p1.addMember(new Manager("Maria", 25));
        p1.addMember(new Programator("Cornel", 30));
        p1.addMember(manager1);
        System.out.println(p1);
    }
}
