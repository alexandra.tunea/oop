//Lectia 2 Prob 5 CV
class Complex{
    private double p_r, p_i;
    public static int contor = 0;

    public Complex(double p_r, double p_i)
    {
        this.p_r = p_r;
        this.p_i = p_i;
    }

    public double Modul(){
        double rez = this.p_r*this.p_r + this.p_i * this.p_i;
        return Math.sqrt(rez);
    }

    public void Print(){
        System.out.println(p_r + " + i * " + p_i);
        contor++;
    }

    public Complex Suma(Complex chestie){
        Complex s;
        s.p_r += chestie.p_r;
        s.p_i += chestie.p_i;
        return s; //Nu vrem sa modificam cu this !!!!
    }

    public static int Count(){
        return contor;
    }

}

public class Prob5{
    public static void main(String[] argv){
        Complex nr1 = new Complex(1.2, 2.5);
        Complex nr2 = new Complex(3, 4);

        nr1.Print();
        nr2.Print();

        System.out.println(nr1.Modul());
        System.out.println(nr2.Modul());

        (nr1.Suma(nr2)).Print();

        int count = Complex.Count();
        System.out.println("Contor = " + count);
    }
}
