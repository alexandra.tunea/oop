import java.util.*;
abstract class Tip{

    public String getTip(){
        return "Tip: ";
    }

    abstract public String toString();
}

class Intreg extends Tip{
    private int atribut;

    public Intreg(int val){
        atribut = val;
    }

    public String getTip( ){
        return super.getTip() + "Intreg";
    }

    public String toString(){
        return "" + atribut;
    }

}

class Sir extends Tip{
    private String atribut;

    public Sir(String val){
        atribut = val;
    }

    public String getTip(){
        return super.getTip() + "Sir";
    }

    public String toString(){
        return "" + atribut;
    }
}

class Colectie extends Tip{
    private List<Tip> lista = new ArrayList<Tip>();
    public void add(Tip o){
        lista.add(o);
    }

    public String toString(){
        Iterator<Tip> it = lista.iterator();
        String s = "";
        while(it.hasNext()){
            s += it.next();
            if(it.hasNext()){
                s += " , ";
            }
        }
        return s;
    }
}

class colectAlex{
        public static void main(String[] args){
            Intreg i1 = new Intreg(25);
            Sir s1 = new Sir("mi e foame");
            //System.out.println(i1);
            //System.out.println(i1.getTip());
            Colectie c = new Colectie();
            c.add(i1);
            c.add(s1);
            //System.out.println(c);
            Colectie c2 = new Colectie();
            c2.add(s1);
            c2.add(i1);
            c.add(c2);
            System.out.println(c);
        }
}
