abstract class Avion{
    private String planeID;
    private int totalEnginePower;

    public Avion(String planeID, int totalEnginePower)
    {
        this.planeID = planeID;
        this.totalEnginePower = totalEnginePower;
    }

    public String getPlaneID(){
        return planeID;
    }

    public int getTotalEnginePower(){
        return totalEnginePower;
    }

    public void takeOff(){
        System.out.println("Plane " + planeID + " - Initiating takeoff procedure - Starting engines - Accelerating down the runway - Taking off - Retracting gear - Takeoff complete");
    }

    public void fly(){
        System.out.println("Plane " + planeID + " - Flying");
    }

    public void land(){
        System.out.println("Plane " + planeID + " initiating landing procedure - Enabling airbrakes -Lowering gear - Contacting runway - Decelerating - Stopping engines - Landing complete");
    }
}

abstract class AvionCalatori extends Avion{
    private int maxPassengers;

    public AvionCalatori(int maxPassengers, String string, int enginePow){
        super(string, enginePow);
        this.maxPassengers = maxPassengers;
    }

    public int getMaxPassengers(){
        return maxPassengers;
    }
}

abstract class AvionLupta extends Avion{

    public AvionLupta(String string, int enginePow){
        super(string, enginePow);
    }

    public void launchMissile(){
        System.out.println("Plane " + getPlaneID() + " initiating missile launch procedure - Acquiring target - Launching missile - Breaking away - Missile launch complet");
    }
}

class Concorde extends AvionCalatori{

    public Concorde(int maxPassengers, String string, int enginePow){
        super(maxPassengers,string,enginePow);
    }

    public void goSuperSonic(){
        System.out.println("Plane " + this.getPlaneID() + " Supersonic mode activated");
    }

    public void goSubSonic(){
        System.out.println("Plane " + this.getPlaneID() + " Supersonic mode DEactivated");
    }
}

class Boeing extends AvionCalatori{
    public Boeing(int maxPassengers, String string, int enginePow){
        super(maxPassengers,string,enginePow);
    }
}

class Mig extends AvionLupta{

    public Mig(String string, int enginePow)
    {
        super(string, enginePow);
    }

    public void highSpeedGeometry(){
        System.out.println("Plane " + this.getPlaneID()+ " - High speed geometry selected");
    }

    public void normalGeometry(){
        System.out.println("Plane " + this.getPlaneID() + " - Normal  geometry selected");
    }
}

class TomCat extends AvionLupta{

    public TomCat(String string, int enginePow)
    {
        super(string, enginePow);
    }

    public void refuel(){
        System.out.println("Plane " + this.getPlaneID()+ "  Initiating refueling procedure - Locating refueller - Catching up -Refueling - Refueling complete");
    }
}


class AvioaneMain{
    public static void main(String[] args){
        Avion[] a = new Avion[4];
        a[0] = new Mig("6734", 4500);
        if(a[0] instanceof Mig)
        ((Mig)a[0]).highSpeedGeometry();

        a[1] = new TomCat("2631b", 6700);
        if(a[1] instanceof TomCat)
        ((TomCat)a[1]).refuel();

        a[2] = new Concorde(150, "2843a",3000);
        if(a[2] instanceof Concorde)
        ((Concorde)a[2]).goSuperSonic();
    }
}
