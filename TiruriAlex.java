class Remorca{
    private int cutii;
    private static int anterior;
    private String nr_inmat;

    public Remorca(int val, String string){
        cutii = val;
        nr_inmat = string;
        this.anterior = val;
    }

    public Remorca(String nr_inmat){
        this.nr_inmat = nr_inmat;
        if(this.anterior == 0){
            cutii = 10;
            this.anterior = cutii;
        }
        else{
            cutii = this.anterior + 1;
            this.anterior = cutii;
        }
    }

    public String afisareRemorca(){
        return "(" + nr_inmat + "," + cutii + ")";
    }

    public int cutiiRemorca(){
        return cutii;
    }

    public String numarRemorca(){
        return nr_inmat;
    }

}


class TiruriAlex{
    Remorca[] tir = new Remorca[5];
    private int current_size;
    public void TiruriALex(){
            current_size = 0;
    }

    public boolean adaugaRemorca(int val, String string){
        if(current_size<=3){
            Remorca remorca_new = new Remorca(val, string);
            tir[current_size] = remorca_new;
            current_size++;
            return true;
        }
        else{
            return false;
        }
    }

    public boolean adaugaRemorca(Remorca remorca_new){
        if(current_size<=3){
            tir[current_size] = remorca_new;
            current_size++;
            return true;
        }
        else{
            return false;
        }
    }

    private int gasesteRemorca(String nr_remorca){
        for(int i=0; i<current_size; i++)
            if((tir[i].numarRemorca()).equals(nr_remorca))
                return i;
        return 25;
    }

    public Remorca stergeRemorca(String nr_remorca){
            int p = this.gasesteRemorca(nr_remorca);
            if(p!=25){
                Remorca ret_val = tir[p];
                for(int i=p; i<current_size; i++)
                    tir[i] = tir[i+1];
                current_size--;
                return ret_val;
            }
            else{
                return null;
            }
    }

    private int cutiiTir(){
        int cutii_total = 0;

        for(int i=0; i<current_size; i++)
            cutii_total += tir[i].cutiiRemorca();

        return cutii_total;
    }

    public boolean egale(TiruriAlex tir2){
        int s1 = this.cutiiTir();
        int s2 = tir2.cutiiTir();

        if(s1==s2){
            return true;
        }
        else{
            return false;
        }
    }

    public void afisareTir(){
        System.out.println("Tirul are remorcile:");
            for(int i=0; i<current_size; i++)
                System.out.println("R" + (i+1) + tir[i].afisareRemorca());
    }

    public static void main(String[] args){
        Remorca r1 = new Remorca(5, "TM01DOA");
        Remorca r2 = new Remorca(6, "CS06EHL");
        Remorca r3 = new Remorca("HD06MRL");
        System.out.println(r3.afisareRemorca());
        TiruriAlex t = new TiruriAlex();
        t.adaugaRemorca(r1);
        t.adaugaRemorca(r2);
        t.adaugaRemorca(10, "TM06ESC");
        //t.stergeRemorca("CS06EHL");
        t.afisareTir();
        TiruriAlex t1 = new TiruriAlex();
        t1.adaugaRemorca(10, "TM09EFG");
        t1.adaugaRemorca(5, "CS78HJL");
        Remorca r4 = new Remorca("AG09KJL");
        t1.adaugaRemorca(r4);
        t1.afisareTir();
        System.out.println(t.egale(t1));
    }
}
