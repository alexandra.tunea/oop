class Fisier{
    private String name;
    private String content;
    private static int id = 1;
    private int id_personal;
    private Fisier prev = null;
    private int concat_count = 0;

    public Fisier(String nume, String continut){
        this.name = nume;
        this.content = continut;
        this.prev = null;
        this.id_personal = id;
        id++;
    }

    public Fisier salveazaVersiune(){
        String new_name  = this.name + "bak";
        Fisier copie = new Fisier(new_name, this.content);
        copie.prev = this.prev;
        this.prev = copie;
        return copie;
    }

    public void afisare(){
        System.out.println("File ID: " + this.id_personal);
        System.out.println("Name : " + this.name);
        System.out.println("Content : " + this.content);
        if(this.prev == null)
            System.out.println("Prev ID: NULL");
        else
            System.out.println("Prev ID: " + this.prev.id_personal);
        System.out.println("------------------");
    }

    public void concateneaza(Fisier file){
        this.salveazaVersiune();
        this.content = this.content + " " +  file.content;
        this.concat_count++;
    }

    public int numarConcatenari(){
        return this.concat_count;
    }

    public String toString(){
        if(this.prev!=null)
            return "ID: " + this.id_personal + " " + "Nume: " + this.name + " " + this.content + " " + "Prev content: " + this.prev.content;
        else
            return "ID: " + this.id_personal + "Nume: " + this.name + this.content;
    }
}

class FisierMain{
    public static void main(String[] argv){
        Fisier file1 = new Fisier("NumeFrumos", "Ana are mere");
        Fisier file2 = null;
        file1.afisare();
        file2 = file1.salveazaVersiune();
        file2.afisare();
        file1.afisare();
        Fisier file3 = new Fisier("NumeFruomos2", " Vr sa mor");
        Fisier file4 = new Fisier("TheLorax", "Womp womp ");
        file2.concateneaza(file3);
        file2.concateneaza(file4);
        file2.afisare();
        int contor = file2.numarConcatenari();
        System.out.println("Contor concatenari: " + contor);
        System.out.println(file2.toString());
    }

}
