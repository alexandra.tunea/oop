import java.io.*;
import java.util.Random;

class Telefon{
    private String name;
    private String[] callers = new String[100];
    private int contor = 0;

    public Telefon(String nume){
        name = nume;
    }

    public boolean apeleaza(Telefon t){
        if(contor<100)
        {
            t.callers[t.contor++]= this.name;
            return true;
        }
        return false;
    }

    public Integer numardeApeluri(String nume){
        int nrApeluri=0;
        for(int i=0; i<contor; i++)
            if(callers[i].equals(nume))
                nrApeluri++;
        return Integer.valueOf(nrApeluri);
    }
    public String toString(){
        String chestie=name + "\n" + "Apeluri:\n";
        for(int i=0; i<contor; i++)
            chestie += callers[i] + "\n";
        return chestie;
    }

    public String afisare()
    {
        return name;
    }
}

class TelefonMain{
    public static void main(String[] args){
           /* Telefon tel1 = new Telefon("Sebastian Popescu");
            Telefon tel2 = new Telefon("Mihnea Remetan");
            Telefon tel3 = new Telefon("Alex Tunea");

            tel1.apeleaza(tel2);
            tel3.apeleaza(tel2);
            tel1.apeleaza(tel2);
            System.out.println(tel2);
            System.out.println(tel2.numardeApeluri("Sebastian Popescu")); */
            int cnt_lines = 0, nr_Tel=0, cnt_Tel = 0, perechi;
            Telefon[] tel = new Telefon[10];
            Random random = new Random();
            try{
            FileInputStream fileInputStream = new FileInputStream("telefoane.txt");
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(fileInputStream));
            String line;
            while((line = inputReader.readLine())!=null){
                if(cnt_lines == 0)
                {
                    nr_Tel = Integer.parseInt(line);
                    tel = new Telefon[nr_Tel];
                    cnt_lines++;
                }
                else if(cnt_lines < nr_Tel + 1)
                {
                    tel[cnt_Tel++] = new Telefon(line);
                    cnt_lines++;
                }
                else if(cnt_lines >= nr_Tel + 1)
                {
                    perechi = Integer.parseInt(line);
                    cnt_lines++;
                    int aux1, aux2;

                    for(int i=0; i<perechi; i++){
                    aux1 = random.nextInt(nr_Tel);
                    aux2 = random.nextInt(nr_Tel);
                    System.out.println("( " + aux1 + " " + aux2 + ")" );

                    while(aux1==aux2){
                    aux1 = random.nextInt(nr_Tel);
                    aux2 = random.nextInt(nr_Tel);
                    System.out.println("( " + aux1 + " " + aux2 + ")" );
                    }
                    tel[aux1].apeleaza(tel[aux2]);
                    }
                }
            }
            }catch(IOException e){
                e.printStackTrace();
            }
            for(int i=0; i<cnt_Tel; i++)
                System.out.println(tel[i]);
        }
}
