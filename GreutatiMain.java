abstract class Greutati{
    abstract public int capacitate();
}

class Simple extends Greutati{
    private int capacitate;

    public Simple(int capacitate){
        this.capacitate = capacitate;
    }

    public int capacitate(){
        return capacitate;
    }
}

class Duble extends Greutati{
    private Greutati g1, g2;

    public Duble(Greutati a, Greutati b){
        g1 = a;
        g2 = b;
    }

    public void setGreutate1(Greutati g){
        g1 = g;
    }

    public void setGreutate2(Greutati g){
        g2 = g;
    }

    public int capacitate(){
        return g1.capacitate() + g2.capacitate();
    }
}

class Multiple extends Greutati{
    private int contor = 0;
    private Greutati[] vector;

    public Multiple(Greutati[] vector_parametru){
        vector = vector_parametru;
    }

    public int capacitate(){
        int  cap=0;
        for(int i=0; i<vector.length; i++)
            if(vector[i]!=null)
            {
                cap += vector[i].capacitate();
            }
        return cap;
    }
}

class ColectieGreutati{
    private Greutati[] v;
    private int contor = 0;
    public ColectieGreutati(int capacitate)
    {
        v = new Greutati[capacitate];
    }

    public void adauga(Greutati g){
        if(contor < v.length)
        {
            v[contor++] = g;
        }
        else
            return;
    }

    public double medie()
    {
        double cap_totala = 0;
        for(int i=0; i<contor; i++)
            cap_totala += v[i].capacitate();
        return (cap_totala/contor);
    }
}

class GreutatiMain{
    public static void main(String[] args){
        Simple g_s1 = new Simple(10);
        Simple g_s2 = new Simple(20);
        System.out.println(g_s1.capacitate());
        Duble g_d1 = new Duble(g_s1, g_s2);
        System.out.println(g_d1.capacitate());
        Greutati[] vector = new Greutati[5];
        vector[0] = g_s1;
        vector[1] = g_s2;
        vector[2] = g_d1;
        Multiple idk = new Multiple(vector);
        System.out.println(idk.capacitate());
        ColectieGreutati colectie = new ColectieGreutati(5);
        colectie.adauga(g_s1);
        colectie.adauga(g_d1);
        colectie.adauga(g_s2);
        colectie.adauga(new Simple(5));
        System.out.println(colectie.medie());
    }
}
