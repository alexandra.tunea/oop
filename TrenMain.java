abstract class Vagon{
    private int pasageri, colete;

    public Vagon(int pasageri, int colete){
        this.pasageri = pasageri;
        this.colete = colete;
    }

    public int nrPasageri(){
        return pasageri;
    }
    public int nrColete(){
        return colete;
    }
}

abstract class VagonTunat extends Vagon{

    public VagonTunat(int pasageri, int colete){
        super(pasageri, colete);
    }

    public void deschidereUsi(){
        System.out.println("Se deschid usile trenului!");
    }

    public void inchideUsi(){
        System.out.println("Se inchid usile trenului!");
    }
}

class CalatoriA extends VagonTunat{
        public CalatoriA(int pasageri, int colete)
        {
            super(40, 300);
        }

}

class CalatoriB extends VagonTunat{
    public CalatoriB(int pasageri, int colete){
        super(50, 400);
    }
    public void blocareGeamuri(){
        System.out.println("S-au blocat geamurile, nu sare nimeni");
    }
}

class Marfa extends Vagon{
    public Marfa(int colete)
    {
        super(0, colete);
    }


    public void dechidereManual(){
        System.out.println("S-au deschis manual geamurile!");
    }
}

class TrenMain{
    public static void main(String[] args){


    }

}

