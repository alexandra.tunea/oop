import java.util.Random;
import java.util.Date;
class CoordinateGenerator {
private Random randomGenerator;
public CoordinateGenerator() {
Date now = new Date();
long sec = now.getTime();
randomGenerator = new Random(sec);
}
public int generateX() {
int x = randomGenerator.nextInt(101);
if(x < 5) {
x = 0;
} else if(x > 95) {
x = 100;
} else {
x = randomGenerator.nextInt(99) + 1;
}
return x;
}
public int generateY() {
int y = randomGenerator.nextInt(101);
if(y < 5) {
y = 0;
} else if(y > 95) {
y = 50;
} else {
y = randomGenerator.nextInt(49) + 1;
}
return y;
}
}
class ExceptieOut extends Exception{
    public ExceptieOut(String mesaj){
        super(mesaj);
    }
}

class ExceptieGol extends Exception{
    public ExceptieGol(String mesaj){
        super(mesaj);
    }
}

class ExceptieCorner extends Exception{
    public ExceptieCorner(String mesaj){
        super(mesaj);
    }
}

class Minge{
    private static CoordinateGenerator gen = new CoordinateGenerator();
    private int x, y;
    public Minge(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    public void suteaza() throws ExceptieOut, ExceptieGol, ExceptieCorner{
        x = gen.generateX();
        y = gen.generateY();
        if(x==0 || y==50){
            throw new ExceptieOut("Avem OUT|");
        }
        else if((x==0 || x==100) && (y>=20 && y<=30)){
            throw new ExceptieGol("GOOOOOOLLLLL|||||");
        }
        else if((x==0 || x==100) &&((0<y && y<20) || (30<y && y<50))){
            throw new ExceptieCorner("Avem corner :/");
        }
    }

    public String toString(){
        String s = "Mingea se afla la coordonatele (" + x + " , " + y + ")";
        return s;
    }
}

class Joc{
    private String echipa1, echipa2;
    private int goluri1=0, goluri2=0, outuri=0, cornere=0;

    public Joc(String s1, String s2){
        echipa1 = s1;
        echipa2 = s2;
    }

    public String toString(){
        String s = echipa1 + "       " + echipa2 + "\n" + goluri1 + "             " + goluri2 + "\n" + "Outuri = " + outuri + "\n" + "Cornere = " + cornere + "\n";
        return s;
    }

    public void comenteaza(Minge m){
        String s = echipa1 + " - " + echipa2 + " : " + m.toString() +"\n" ;
        System.out.print(s);
    }

    public void simuleaza(){
        //int x = gen.generateX();
        //int y = gen.generateY();
        int x, y;
        Minge minge = new Minge( 50,  25);
        comenteaza(minge);

        for(int i=0; i<5; i++){
        try{
            minge.suteaza();
        }catch(ExceptieOut e){
            Minge mingeNoua = new Minge(minge.getX(), minge.getY());
            minge = mingeNoua;
            outuri++;
        }catch(ExceptieGol e){
            if(minge.getX()==0){
                goluri2++;
            }
            if(minge.getX()==100){
                goluri1++;
            }
            x=50; y=25;
            minge = new Minge(x, y);
        }catch(ExceptieCorner e){
            cornere++;
            minge = new Minge(0, 0);
        }finally{
            comenteaza(minge);
        }
        }
        System.out.println(this);
    }
}

class Fotbal{
    public static void main(String[] args){
        Joc joc1 = new Joc("CF Bozovici", "Atletico Textila");
        joc1.simuleaza();
    }
}
